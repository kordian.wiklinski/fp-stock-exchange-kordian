import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FpUserRegisterComponent } from './fp-user-register.component';

describe('FpUserRegisterComponent', () => {
  let component: FpUserRegisterComponent;
  let fixture: ComponentFixture<FpUserRegisterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FpUserRegisterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FpUserRegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
