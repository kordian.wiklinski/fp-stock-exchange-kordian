import { Component, OnInit } from '@angular/core';
import { RouterModule, Routes }   from '@angular/router';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';
import { Router }   from '@angular/router';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Subscription }   from 'rxjs/Subscription';

//Services
import { FpUserService } from '../services/fp-user.service';
import { FpGlobalScopesService } from '../services/fp-global-scopes.service';

@Component({
  selector: 'app-fp-user-register',
  templateUrl: './fp-user-register.component.html',
  styleUrls: ['./fp-user-register.component.less']
})
export class FpUserRegisterComponent implements OnInit {

  constructor(
      private afAuth : AngularFireAuth,
      private http : Http,
      private afDatabase : AngularFireDatabase,
      private router : Router,
      private gs: FpGlobalScopesService,
      private us: FpUserService
  ) {}

  ngOnInit() {
  }

  preapretoRegisterUser(){
    if (this.us.newUser.username && this.us.newUser.email && this.us.newUser.password && this.us.newUser.password.length > 5) {
      this.us.registerUser(this.us.newUser);
    }
  }

}
