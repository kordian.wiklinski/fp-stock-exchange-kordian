import { Component, OnInit } from '@angular/core';
import { RouterModule, Routes }   from '@angular/router';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';
import { Router }   from '@angular/router';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Subscription }   from 'rxjs/Subscription';

//Services
import { FpUserService } from '../services/fp-user.service';
import { FpGlobalScopesService } from '../services/fp-global-scopes.service';
import { FpStockExchangeService } from '../services/fp-stock-exchange.service'

declare var moment: any;

@Component({
  selector: 'app-fp-stock-exchange-panel',
  templateUrl: './fp-stock-exchange-panel.component.html',
  styleUrls: ['./fp-stock-exchange-panel.component.less']
})

export class FpStockExchangePanelComponent implements OnInit {

  stockExchangeSubscription: Subscription;

  constructor(
      private afAuth : AngularFireAuth,
      private http : Http,
      private afDatabase : AngularFireDatabase,
      private router : Router,
      private gs: FpGlobalScopesService,
      private us: FpUserService,
      private stockExchange: FpStockExchangeService) { }

  ngOnInit() {
    this.stockExchange.getStocks();
    let __this = this;
    let getStocksInterval = setInterval(function(){
      __this.stockExchange.getStocks();
    }, __this.stockExchange.updateStocks);
  }

  buyStocks(singleStock: any): void {
    this.stockExchange.stockToBuy = JSON.parse(JSON.stringify(this.stockExchange.stockTransaction));
    this.stockExchange.isBuying = true;
    this.stockExchange.isSelling = false;
    this.stockExchange.stockToBuy['name'] = singleStock['name'];
    this.stockExchange.stockToBuy['code'] = singleStock['code'];
    this.stockExchange.stockToBuy['price'] = singleStock['price'];

  }
  sellStocks(singleStock: any): void {
    this.stockExchange.stockToSell = JSON.parse(JSON.stringify(this.stockExchange.stockTransaction));
    this.stockExchange.isSelling = true;
    this.stockExchange.isBuying = false;
    this.stockExchange.stockToSell['name'] = singleStock['name'];
    this.stockExchange.stockToSell['code'] = singleStock['code'];

    for (let i = 0; i < this.gs.stocks.items.length; i++){
      if (this.gs.stocks.items[i]['code'] === singleStock['code']) {
        this.stockExchange.stockToSell['price'] = this.gs.stocks.items[i]['price'];
      }
    }
  }

}
