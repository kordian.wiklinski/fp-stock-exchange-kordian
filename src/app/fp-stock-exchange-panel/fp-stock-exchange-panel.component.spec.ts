import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FpStockExchangePanelComponent } from './fp-stock-exchange-panel.component';

describe('FpStockExchangePanelComponent', () => {
  let component: FpStockExchangePanelComponent;
  let fixture: ComponentFixture<FpStockExchangePanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FpStockExchangePanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FpStockExchangePanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
