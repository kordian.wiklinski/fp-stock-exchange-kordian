import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FpUserTopNavigationComponent } from './fp-user-top-navigation.component';

describe('FpUserTopNavigationComponent', () => {
  let component: FpUserTopNavigationComponent;
  let fixture: ComponentFixture<FpUserTopNavigationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FpUserTopNavigationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FpUserTopNavigationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
