import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FpStockExchangeModalsComponent } from './fp-stock-exchange-modals.component';

describe('FpStockExchangeModalsComponent', () => {
  let component: FpStockExchangeModalsComponent;
  let fixture: ComponentFixture<FpStockExchangeModalsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FpStockExchangeModalsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FpStockExchangeModalsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
