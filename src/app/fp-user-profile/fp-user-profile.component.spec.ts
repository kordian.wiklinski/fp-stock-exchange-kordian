import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FpUserProfileComponent } from './fp-user-profile.component';

describe('FpUserProfileComponent', () => {
  let component: FpUserProfileComponent;
  let fixture: ComponentFixture<FpUserProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FpUserProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FpUserProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
