import { Component, OnInit } from '@angular/core';
import { RouterModule, Routes }   from '@angular/router';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';
import { Router }   from '@angular/router';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Subscription }   from 'rxjs/Subscription';

//Services
import { FpUserService } from '../services/fp-user.service';
import { FpGlobalScopesService } from '../services/fp-global-scopes.service';
import { FpStockExchangeService } from '../services/fp-stock-exchange.service'

@Component({
  selector: 'app-fp-user-profile',
  templateUrl: './fp-user-profile.component.html',
  styleUrls: ['./fp-user-profile.component.less']
})
export class FpUserProfileComponent implements OnInit {

  updateUser: any = {};

  constructor(
      private afAuth : AngularFireAuth,
      private http : Http,
      private afDatabase : AngularFireDatabase,
      private router : Router,
      private gs: FpGlobalScopesService,
      private us: FpUserService,
      private stockExchange: FpStockExchangeService
  ) { }

  ngOnInit() {
    this.stockExchange.getStocks();

    this.us.checkAuth().subscribe(auth => {
      this.gs.activeUser = JSON.parse(JSON.stringify(auth));
      if (this.gs.activeUser && typeof this.gs.activeUser['details'] === 'undefined') {
        // this.us.userProfileInfo(this.gs.activeUser.uid);
        let refUser = this.us.userProfileInfoSubscription(this.gs.activeUser.uid);
        refUser.subscribe(response => {
          this.gs.activeUser['details'] = response;
          this.updateUser = {
            username: this.gs.activeUser.details.username,
            email: this.gs.activeUser.details.email,
            wallet: this.gs.activeUser.details.wallet,
          }
        }, error => {
          // console.log(error);
        });
      }
    });
  }

  updateUserProfile() {
    this.updateUser['updatedat'] = new Date().toISOString();
    let updateUser = this.afDatabase.object('users/' + this.gs.activeUser.uid);
    updateUser.update(this.updateUser).then(response => {
      this.gs.alert = {
        type: 'success',
        message: 'Success profile update'
      }
    }, error => {
      this.gs.alert = {
        type: 'warning',
        message: 'Problem in update the profile'
      }
    });
  }
  
  restoreStocks(): void {
    for (let i = 0; i < this.gs.stocks.items.length; i++) {
      this.stockExchange.restoreSingleStock(this.gs.stocks.items[i]);
    }

    this.gs.alert = {
      type: 'success',
      message: 'Stocks updated'
    }
  }

}
