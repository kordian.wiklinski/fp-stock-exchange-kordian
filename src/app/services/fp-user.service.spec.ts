import { TestBed, inject } from '@angular/core/testing';

import { FpUserService } from './fp-user.service';

describe('FpUserService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FpUserService]
    });
  });

  it('should be created', inject([FpUserService], (service: FpUserService) => {
    expect(service).toBeTruthy();
  }));
});
