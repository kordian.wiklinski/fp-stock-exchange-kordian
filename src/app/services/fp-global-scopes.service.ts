import { Injectable } from '@angular/core';

@Injectable()
export class FpGlobalScopesService {
  
  activeUser: any = null; // user object after logged in will be updated
  stocks: any = null; // all global stocks of all companies
  stockUpdate: any = null; // datetime when stock were updated

  // alert and notifications
  alert: any = {
    type: null,
    message: null
  }

  constructor() { }

}
