import { TestBed, inject } from '@angular/core/testing';

import { FpGlobalScopesService } from './fp-global-scopes.service';

describe('FpGlobalScopesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FpGlobalScopesService]
    });
  });

  it('should be created', inject([FpGlobalScopesService], (service: FpGlobalScopesService) => {
    expect(service).toBeTruthy();
  }));
});
