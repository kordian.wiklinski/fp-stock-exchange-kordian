import { TestBed, inject } from '@angular/core/testing';

import { FpStockExchangeService } from './fp-stock-exchange.service';

describe('FpStockExchangeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FpStockExchangeService]
    });
  });

  it('should be created', inject([FpStockExchangeService], (service: FpStockExchangeService) => {
    expect(service).toBeTruthy();
  }));
});
