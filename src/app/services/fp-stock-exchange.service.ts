import { Injectable, OnInit, OnDestroy } from '@angular/core';
import { AngularFireDatabaseModule, AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database';
import { AngularFireAuthModule, AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { Router }   from '@angular/router';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Subscription }   from 'rxjs/Subscription';
import { Observable } from 'rxjs/Rx';;

//Services
import { FpUserService } from '../services/fp-user.service';
import { FpGlobalScopesService } from '../services/fp-global-scopes.service';

declare var moment: any;

@Injectable()
export class FpStockExchangeService {

  private url = 'http://localhost:3000/stocks';
  public stocks: any = null;
  public stocksConnectionProblem: boolean = false;
  public updateStocks: number = 20 * 1000; // 120 secunds
  private stockExchangeSubscription: Subscription;
  private getStocksNumberSubscription: FirebaseObjectObservable<any>;

  public isBuying: boolean = false;
  public isSelling: boolean = false;

  public stockTransaction = {
    price: 0,
    number: 0,
    bought: 1,
    totalPrice: 0
  }
  public stockToBuy: any = {}
  public stockToSell: any = {}

  constructor(
      private afAuth : AngularFireAuth,
      private http : Http,
      private afDatabase : AngularFireDatabase,
      private router : Router,
      private gs: FpGlobalScopesService
  ) { }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.stockExchangeSubscription.unsubscribe();
    this.getStocksNumberSubscription = null;
  }

  getStocks(): void {
    let headers = new Headers({ 'Accept': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    this.stockExchangeSubscription = this.http.get(this.url).subscribe(response => {

      this.gs.stocks = response = response.json();
      this.gs.stocks['publicationDate'] = this.gs.stocks['publicationDate'];
      this.gs.stocks['publicationDateMoment'] = moment(this.gs.stocks['publicationDate']).format("DD/MM/YYYY HH:MM:ss");

      if (this.gs.stocks['publicationDate']) {
        this.stocksConnectionProblem = false;
        
        this.getStocksNumber();
      } else {
        this.stocksConnectionProblem = true;
        this.gs.alert = {
          type: 'warning',
          message: 'Problem with stock exchange connection'
        }
      }

    }, error => {
      console.log(error);
    });

  }

  getStocksSub(): Observable<Response> {
    let headers = new Headers({ 'Accept': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    return this.http.get(this.url);

  }

  getStocksNumber(): void {
    this.getStocksNumberSubscription = this.afDatabase.object('/stocks');
    this.getStocksNumberSubscription.subscribe(response => {

      this.gs.stocks['globalStocks'] = response;
      // this.gs.stocks
    }, error => {
      // console.log(error);
    });
  }

  restoreSingleStock(stock: any): void{
    let refStock = this.afDatabase.database.ref('stocks/' + stock['code']);
    let createdDate = new Date();
    let stocksNumber = {
      number: 1000000,
      code: stock.code,
      name: stock.name,
      updatedat: new Date().toISOString()
    }
    refStock.set(stocksNumber);
  }

  buyStocksByUser(stockToBuy: any): void{

    this.gs.stockUpdate = JSON.parse(JSON.stringify(this.gs.stocks['publicationDate']));
    this.getStocksSub().subscribe(response => {
      response = response.json();

      let stockNotDeprecated = (this.gs.stockUpdate === response['publicationDate']);
      if (stockNotDeprecated) {
        this.buyStockValidate(stockToBuy);
      } else {
        this.gs.alert = {
          type: 'warning',
          message: 'Stocks data changes, please re buy'
        }
        this.getStocks();
        return;
      }
    }, error => {
      console.log(error);
    })
  }

  buyStockValidate(stockToBuy: any) {

    // cant buy stock with value less than zero
    let amountLessThanZero = stockToBuy.bought <= 0;
    // check if total price is bigger than money in wallet
    let totalPriceBiggerThanWallet = parseInt((stockToBuy.price * stockToBuy.bought).toFixed(2)) > this.gs.activeUser.details.wallet;
    // check if amount to stock to buy is bigger than available value
    let amountOfStockBiggerThanGlobal = (stockToBuy.bought > this.gs.stocks.globalStocks[stockToBuy.code].number);

    if (amountLessThanZero) {
      this.gs.alert = {
        type: 'warning',
        message: 'Amount of stocks is less than zero'
      }
      return;
    }
    if (totalPriceBiggerThanWallet) {
      this.gs.alert = {
        type: 'warning',
        message: 'You dont have enough money in you wallet'
      }
      return;
    }
    if (amountOfStockBiggerThanGlobal) {
      this.gs.alert = {
        type: 'warning',
        message: 'You want to buy more stock than are on stock exchange'
      }
      return;
    }

    if (confirm('Confirm to buy stocks')) {
      this.buyStockProcess(stockToBuy);
    }

  }

  buyStockProcess(stockToBuy: any): void {
    stockToBuy.totalPrice = parseInt((stockToBuy.price * stockToBuy.bought).toFixed(2));

    // users stocks
    let refStock = this.afDatabase.database.ref('stocks-users/' + this.gs.activeUser['uid'] + '/' + stockToBuy.code );
    // user wallet
    let refUserWallet = this.afDatabase.database.ref('users/' + this.gs.activeUser['uid'] + '/wallet');
    // total number of company's stocks
    let refStockGlobal = this.afDatabase.database.ref('stocks/' + stockToBuy.code + '/number');

    // check if user already have stocks
    if (typeof (this.gs.activeUser['stocks'][stockToBuy['code']]) !== 'undefined') {
      let stocksBought = this.gs.activeUser['stocks'][stockToBuy['code']];
      stocksBought['number'] += parseInt(stockToBuy['bought']);
      stocksBought['updatedat'] =  new Date().toISOString();
      refStock.update(stocksBought);

    } else {
      let createdDate = new Date();
      let stocksBought = {
        number: stockToBuy['bought'],
        code: stockToBuy.code,
        name: stockToBuy.name,
        createdat: new Date().toISOString()
      }
      refStock.set(stocksBought);
    }

    // update wallet
    refUserWallet.set(this.gs.activeUser.details.wallet - stockToBuy.totalPrice);
    //update number of stocks of company

    let stockGlobalAmount = this.gs.stocks.globalStocks[stockToBuy.code].number - stockToBuy.bought;
    if (stockGlobalAmount < 1) {
      stockGlobalAmount = 0;
    }
    refStockGlobal.set(stockGlobalAmount);


    this.gs.alert = {
      type: 'success',
      message: 'Bought ' + stockToBuy.bought + ' of ' + stockToBuy.code + ' for ' + stockToBuy.totalPrice + ' PLN'
    }

    this.isBuying = false;
  }

  sellStocksByUser(stockToSell: any): void{

    this.gs.stockUpdate = JSON.parse(JSON.stringify(this.gs.stocks['publicationDate']));
    this.getStocksSub().subscribe(response => {
      response = response.json();
      let stockNotDeprecated = (this.gs.stockUpdate == response['publicationDate']);
      if (stockNotDeprecated) {
        this.sellStockValidate(stockToSell);
      } else {
        this.gs.alert = {
          type: 'warning',
          message: 'Stocks data changes, please re sell'
        }
        this.getStocks();
        return;
      }
    }, error => {
      console.log(error);
    })
  }

  sellStockValidate(stockToSell: any) {

    // cant sell stock with value less than zero
    let amountLessThanZero = stockToSell.bought <= 0;
    // check if amount to stock to sell is bigger than available value
    let amountOfStockBiggerThanGlobal = (stockToSell.bought > this.gs.activeUser.stocks[stockToSell.code].number);

    if (amountLessThanZero) {
      this.gs.alert = {
        type: 'warning',
        message: 'Amount of stocks is less than zero'
      }
      return;
    }
    if (amountOfStockBiggerThanGlobal) {
      this.gs.alert = {
        type: 'warning',
        message: 'You want to sell more stock than you have'
      }
      return;
    }

    if (confirm('Confirm to sell stocks')) {
      this.sellStockProcess(stockToSell);
    }

  }

  sellStockProcess(stockToSell: any): void {
    stockToSell.totalPrice = parseInt((stockToSell.price * stockToSell.bought).toFixed(2));

    // users stocks
    let refStock = this.afDatabase.database.ref('stocks-users/' + this.gs.activeUser['uid'] + '/' + stockToSell.code );
    // user wallet
    let refUserWallet = this.afDatabase.database.ref('users/' + this.gs.activeUser['uid'] + '/wallet');
    // total number of company's stocks
    let refStockGlobal = this.afDatabase.database.ref('stocks/' + stockToSell.code + '/number');

    // check if user already have stocks

    //remove stock-users code node if stocks are equall to zero
    let noStocks = (stockToSell.bought >= this.gs.activeUser.stocks[stockToSell.code].number);
    if (noStocks) {
      refStock.remove();

    } else {
      let stocksBought = this.gs.activeUser['stocks'][stockToSell['code']];
      stocksBought['number'] -= parseInt(stockToSell['bought']);
      stocksBought['updatedat'] =  new Date().toISOString();
      refStock.update(stocksBought);
    }

    // update wallet
    refUserWallet.set(this.gs.activeUser.details.wallet + stockToSell.totalPrice);
    //update number of stocks of company
    refStockGlobal.set(this.gs.stocks.globalStocks[stockToSell.code].number + stockToSell.bought);

    this.gs.alert = {
      type: 'success',
      message: 'Sold ' + stockToSell.bought + ' of ' + stockToSell.code + ' for ' + stockToSell.totalPrice + ' PLN'
    }

    this.isSelling = false;
  }

}