import { Injectable, OnInit, OnDestroy } from '@angular/core';
import { AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { Router }   from '@angular/router';
import { Http } from '@angular/http';
import { Subscription }   from 'rxjs/Subscription';
import { Observable } from 'rxjs/Rx';

//Services
import { FpGlobalScopesService } from './fp-global-scopes.service';
import {useAnimation} from "@angular/animations/animations";

@Injectable()
export class FpUserService {

    newUser: any = {
        username:  null,
        email:  null,
        password:  null,
        wallet:  10
    };

    activeUserSubscription: Subscription;

    authSub: Subscription;
    refUser: FirebaseObjectObservable<any>;

    constructor(
        private afAuth : AngularFireAuth,
        private http : Http,
        private afDatabase : AngularFireDatabase,
        private router : Router,
        private gs: FpGlobalScopesService
    ) {
        this.activeUserSubscription = this.afAuth.authState.subscribe(auth => {
            this.gs.activeUser = JSON.parse(JSON.stringify(auth));
            if (this.gs.activeUser && typeof this.gs.activeUser['details'] === 'undefined') {
                this.userProfileInfo(this.gs.activeUser.uid);
            }
        });
    }

    OnDestroy() {
        this.authSub.unsubscribe();
        this.refUser = null;
    }

    checkAuth(): Observable<firebase.User> {
        return this.afAuth.authState;
    }

    registerUser(newUser: any): void{
        this.afAuth.auth.createUserWithEmailAndPassword(newUser.email, newUser.password).then(
            response => {
                this.gs.alert = {
                    type: 'success',
                    message: 'New user registered'
                }
                
                this.saveNewUserData(response.uid, newUser.username, newUser.email, newUser.wallet);
                this.router.navigate(['/']);
                
            }, error => {
                console.log(error);
                this.gs.alert = {
                    type: 'warning',
                    message: 'Unable to register new user'
                }
            }
        );
    }

    login(email : string , password : string) : void {
        this.afAuth.auth.signInWithEmailAndPassword(email, password )
            .then(response => {
                this.gs.alert = {
                    type: 'success',
                    message: 'Success logged in'
                }
                this.router.navigate(['/']);
            }, error => {
                console.log(error);
                this.gs.alert = {
                    type: 'warning',
                    message: error['message']
                }
            });
    }

    logout() : void {
        this.refUser = null;
        this.afAuth.auth.signOut();
        this.gs.activeUser = null;
        this.router.navigate(['/']);
    }

    saveNewUserData(uid: string, newUserName: string, newUserEmail: string, wallet: number): void {
        let refUsers = this.afDatabase.database.ref('users/' + uid);
        let createdDate = new Date();
        let userToSave = {
            username: newUserName,
            email: newUserEmail,
            createdat: createdDate.toDateString(),
            wallet: wallet
        }
        refUsers.set(userToSave);
    }

    userProfileInfo(uid: string) {
        this.refUser = this.afDatabase.object('users/' + uid);
        this.refUser.subscribe(response => {
            this.gs.activeUser['details'] = response;

            this.getUserStocks();
        }, error => {
            // console.log(error);
        });
    }
    userProfileInfoSubscription(uid: string): Observable<any> {
        return this.afDatabase.object('users/' + uid);
    }


    getUserStocks(): void {
        let userStocks = this.afDatabase.object('stocks-users/' + this.gs.activeUser['uid']);
        this.gs.activeUser['stocksArray'] = [];
        userStocks.subscribe(response => {
            this.gs.activeUser['stocks'] = response;
            for (let i in this.gs.activeUser.stocks) {
                if (this.gs.activeUser.stocks[i]) {
                    this.gs.activeUser['stocksArray'].push(this.gs.activeUser.stocks[i]);
                }
            }
        }, error => {
            // console.log(error);
        })
    }

}
