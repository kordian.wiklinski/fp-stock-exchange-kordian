import { Component, OnInit, OnDestroy, OnChanges } from '@angular/core';

//Services
import { FpUserService } from '../services/fp-user.service';
import { FpGlobalScopesService } from '../services/fp-global-scopes.service';

@Component({
  selector: 'app-fp-alerts',
  templateUrl: './fp-alerts.component.html',
  styleUrls: ['./fp-alerts.component.less']
})
export class FpAlertsComponent implements OnInit {

  constructor(
      private gs: FpGlobalScopesService,
      private us: FpUserService) { }

  ngOnInit() {
  }
  ngOnDestroy() {
    this.gs.alert.type = null;
    this.gs.alert.message = null;
  }

}
