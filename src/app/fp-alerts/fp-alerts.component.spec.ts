import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FpAlertsComponent } from './fp-alerts.component';

describe('FpAlertsComponent', () => {
  let component: FpAlertsComponent;
  let fixture: ComponentFixture<FpAlertsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FpAlertsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FpAlertsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
