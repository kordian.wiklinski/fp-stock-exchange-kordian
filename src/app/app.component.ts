import { Component } from '@angular/core';
import { FpUserService } from './services/fp-user.service';
import { FpGlobalScopesService } from './services/fp-global-scopes.service';

//3rd party

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {

  constructor(
      public fpUser: FpUserService,
      public gs: FpGlobalScopesService
  ){}

}
