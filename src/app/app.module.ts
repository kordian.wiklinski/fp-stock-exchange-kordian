import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes }   from '@angular/router';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';

import { firebaseConfig } from './config';

//Services
import { FpUserService } from './services/fp-user.service';
import { FpGlobalScopesService } from './services/fp-global-scopes.service';
import { FpStockExchangeService } from './services/fp-stock-exchange.service';

//Components
import { AppComponent } from './app.component';
import { FpUserRegisterComponent } from './fp-user-register/fp-user-register.component';
import { FpWelcomePageComponent } from './fp-welcome-page/fp-welcome-page.component';
import { FpUserTopNavigationComponent } from './fp-user-top-navigation/fp-user-top-navigation.component';
import { FpUserLoginComponent } from './fp-user-login/fp-user-login.component';
import { FpAlertsComponent } from './fp-alerts/fp-alerts.component';
import { FpUserProfileComponent } from './fp-user-profile/fp-user-profile.component';
import { FpStockExchangePanelComponent } from './fp-stock-exchange-panel/fp-stock-exchange-panel.component';
import { FpStockExchangeModalsComponent } from './fp-stock-exchange-modals/fp-stock-exchange-modals.component';

const appRoutes: Routes = [
  { path: 'register', component: FpUserRegisterComponent},
  { path: 'login', component: FpUserLoginComponent},
  { path: 'profile', component: FpUserProfileComponent},
  { path: '**', component: FpWelcomePageComponent}
  ];

@NgModule({
  declarations: [
    AppComponent,
    FpUserRegisterComponent,
    FpWelcomePageComponent,
    FpUserTopNavigationComponent,
    FpUserLoginComponent,
    FpAlertsComponent,
    FpUserProfileComponent,
    FpStockExchangePanelComponent,
    FpStockExchangeModalsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AngularFireModule.initializeApp(firebaseConfig.firebase),
    RouterModule.forRoot(appRoutes),
  ],
  providers: [
    FpUserService,
    FpGlobalScopesService,
    FpStockExchangeService,
    AngularFireAuth,
    AngularFireDatabase
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
