import { Component, OnInit } from '@angular/core';
import { RouterModule, Routes }   from '@angular/router';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';
import { Router }   from '@angular/router';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Subscription }   from 'rxjs/Subscription';

//Services
import { FpUserService } from '../services/fp-user.service';
import { FpGlobalScopesService } from '../services/fp-global-scopes.service';

@Component({
  selector: 'app-fp-user-login',
  templateUrl: './fp-user-login.component.html',
  styleUrls: ['./fp-user-login.component.less']
})
export class FpUserLoginComponent implements OnInit {

  loginUser: any = {
    email:  null,
    password:  null
  };

  constructor(
      private afAuth : AngularFireAuth,
      private http : Http,
      private afDatabase : AngularFireDatabase,
      private router : Router,
      private gs: FpGlobalScopesService,
      private us: FpUserService) { }

  ngOnInit() {
  }


  preapretoLoginUser(){
    if (this.loginUser.email && this.loginUser.password && this.loginUser.password.length > 5) {
      this.us.login(this.loginUser.email, this.loginUser.password);
    }
  }

}
