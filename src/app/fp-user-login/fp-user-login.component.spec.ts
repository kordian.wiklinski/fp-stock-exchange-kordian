import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FpUserLoginComponent } from './fp-user-login.component';

describe('FpUserLoginComponent', () => {
  let component: FpUserLoginComponent;
  let fixture: ComponentFixture<FpUserLoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FpUserLoginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FpUserLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
