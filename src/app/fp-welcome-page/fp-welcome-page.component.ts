import { Component, OnInit } from '@angular/core';
import { RouterModule, Routes }   from '@angular/router';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';
import { Router }   from '@angular/router';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Subscription }   from 'rxjs/Subscription';

//Services
import { FpUserService } from '../services/fp-user.service';
import { FpGlobalScopesService } from '../services/fp-global-scopes.service';
import { FpStockExchangeService } from '../services/fp-stock-exchange.service'

@Component({
  selector: 'app-fp-welcome-page',
  templateUrl: './fp-welcome-page.component.html',
  styleUrls: ['./fp-welcome-page.component.less']
})
export class FpWelcomePageComponent implements OnInit {

  constructor(
      private afAuth : AngularFireAuth,
      private http : Http,
      private afDatabase : AngularFireDatabase,
      private router : Router,
      private gs: FpGlobalScopesService,
      private us: FpUserService,
      private stockExchange: FpStockExchangeService) { }

  ngOnInit() {
    if (this.gs.activeUser && typeof this.gs.activeUser['stocks'] === 'undefined'){
      this.us.userProfileInfo(this.gs.activeUser.uid);
    };
  }

}
