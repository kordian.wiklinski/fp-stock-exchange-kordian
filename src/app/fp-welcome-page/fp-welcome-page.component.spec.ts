import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FpWelcomePageComponent } from './fp-welcome-page.component';

describe('FpWelcomePageComponent', () => {
  let component: FpWelcomePageComponent;
  let fixture: ComponentFixture<FpWelcomePageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FpWelcomePageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FpWelcomePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
