import { FpStockExchangeKordianPage } from './app.po';

describe('fp-stock-exchange-kordian App', () => {
  let page: FpStockExchangeKordianPage;

  beforeEach(() => {
    page = new FpStockExchangeKordianPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
