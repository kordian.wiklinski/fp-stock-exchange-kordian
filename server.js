/**
 * Created by kordian on 27.08.17.
 */
// Get dependencies
const express = require('express');
const path = require('path');
const http = require('http');
const bodyParser = require('body-parser');
var request = require('request');

const app = express();

// Parsers for POST data
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.get('/stocks', (req, res) => {
  var options = {
    host: 'http://webtask.future-processing.com:8068',
    path: '/stocks'
  };

  var __res = res;

  request({
    headers: {
      'Accept': 'application/json'
    },
    uri: 'http://webtask.future-processing.com:8068/stocks',
    method: 'GET'
  }, function (err, res, body) {
    __res.setHeader('Access-Control-Allow-Origin', '*')
    if (!err) {
      __res.send(body);
    } else {
      __res.send(err);
    }

  });
});

/**
 * Get port from environment and store in Express.
 */
const port = process.env.PORT || '3000';
app.set('port', port);

/**
 * Create HTTP server.
 */
const server = http.createServer(app);

/**
 * Listen on provided port, on all network interfaces.
 */
server.listen(port, () => console.log(`API running on localhost:${port}`));